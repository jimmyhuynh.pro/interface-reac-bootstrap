import { Request, Response } from "express-serve-static-core";
import CompetencesController from "./CompetenceController";
import HomeController from "./HomeController";

export default class CriteresController {

    static cocheUnCritere(req: Request, res: Response): void {
        const result = req.body;

        const db = req.app.locals.db;

        const criteres = db.prepare('SELECT * FROM criteres WHERE competences_idcompetences = ?').all(result.compId);

        const recup = db.prepare('SELECT * FROM user_has_criteres').all();

        const criteresAlreadyChecked: number[] = []
        recup.forEach((element: any) => criteresAlreadyChecked.push(element.criteres_idcriteres));
        console.log(criteresAlreadyChecked)
        
        for (let critere of criteres) {
            const critId = critere.idcriteres;

            let checkboxId: number = req.body['checkbox' + critId];
            //console.log (req.body['checkbox' + critId])
            if (checkboxId !== undefined)
            {
                if (!criteresAlreadyChecked.includes(+checkboxId)) {
                    db.prepare("INSERT INTO user_has_criteres (user_idUser, criteres_idcriteres) VALUES (?, ?)").run(1, critId);
                }
                else
                {
                    console.log('critere est deja coché')
                }
            }
            else if (criteresAlreadyChecked.includes(critId))
            {
                db.prepare("DELETE FROM user_has_criteres WHERE user_idUser = ? AND criteres_idcriteres = ? ").run(1, critId)
            }
        }
 
        // for (let element of criteres){
        //     console.log (element.idcriteres)
        //     if(criteresAlreadyChecked.includes(element.idcriteres)){
        //         element.checked ='checked'
        //     }
        //     else{
        //         element.checked = ''
        //     }
        // }
        HomeController.index(req, res);
    }
}