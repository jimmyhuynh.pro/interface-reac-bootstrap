import { Request, Response } from "express-serve-static-core";
import { appendFile } from "fs";
import bcrypt, { hash } from "bcrypt";

export default class HomeController {

    static index(req: Request, res: Response): void {
        const db = req.app.locals.db;

        const stmt = db.prepare('SELECT * FROM competences WHERE idcompetences < 5').all();
        const stmt2 = db.prepare('SELECT * FROM competences WHERE idcompetences > 4').all();

        res.render('pages/index', {
            competence: stmt,
            competence2: stmt2,
        });
    }

    static about(req: Request, res: Response): void {
        res.render('pages/about', {
            title: 'About',
        });
    }

    static register(req: Request, res: Response): void {
        const db = req.app.locals.db;
        const hash = bcrypt.hashSync(req.body.password, 10);
        const register = db.prepare('INSERT INTO user ("username","password","email") VALUES (?,?,?)').run(req.body.username, hash, req.body.email)


        res.render('pages/login', {


        })
    }

    static login(req: Request, res: Response): void {
        res.render('pages/login', {

        })
    }
}