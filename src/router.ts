import { Application } from "express";
import express from "express";
import HomeController from "./controllers/HomeController";
import CompetencesController from "./controllers/CompetenceController";
import CriteresController from "./controllers/criteresController";
import { body, validationResult } from "express-validator";
const bcrypt = require("bcrypt");
const cookieParser = require("cookie-parser");
const sessions = require('express-session');

export default function route(app: Application) {
    /** Static pages **/
    app.get('/', (req, res) => {
        HomeController.index(req, res);
    });

    app.get('/about', (req, res) => {
        HomeController.about(req, res);
    });


    // affiche le form
    app.get('/competences/:id', (req, res) => {

        CompetencesController.competences(req, res);
    });

    // recup le form
    app.post('/criteres', (req, res) => {

        CriteresController.cocheUnCritere(req, res)
    });


    app.post('/register', (req, res) => {

        HomeController.register(req, res);
    });

    app.get('/register', (req, res) => {

        res.render('pages/register'),{

        };
    });

    // app.post('/register',
        
    //     body('username').isLength({ 
    //         min: 6 }),
    //     body('password').isLength({ 
    //         min: 6 }),
    //     body('email').isEmail().normalizeEmail(),
    //     (req: express.Request, res: express.Response) => {
    //         const errors = validationResult(req);
    
    //         if (!errors.isEmpty()) {
    //                 return res.status(400).json({
    //                     success: false,
    //                     errors: errors.array()
    //                 });
        
    //         HomeController.register(req, res);
    //     }
    // });

    // app.post('/register', async (req, res) => {
    //     const db = req.app.locals.db;
    //     try {
    //         const {username, password, email} = req.body;
    //         const hash = await bcrypt.hash(password, 10);
    //         await db('user').insert({username : username, hash : hash, email : email});
    //         res.status(200).json('Ca marche')
    //     } catch(e) {
    //         console.log(e);
    //         res.status(500).send('Il y a un problème');
    //     }

    //     HomeController.register(req, res);
    // });

    app.get('/login', (req, res) => {

        HomeController.login(req, res);
    });

    // app.post('/user',(req,res) => {
    //     if(req.body.username == username && req.body.password == password){
    //         session=req.session;
    //         session.userid=req.body.username;
    //         console.log(req.session)
    //         res.send(`Hey there, welcome <a href=\'/logout'>click to logout</a>`);
    //     }
    //     else{
    //         res.send('Invalid username or password');
    //     }
    // })
}