# Interface REAC 



## Journal de bord

##Jour 1 (06/12)

Prévu : construire la base de donnée (tables et clés pimaire).

Produit : la base de donnée.

Problèmes : rien de particulier.

##Jour 2 (07/12)

Prévu : création de la base de donnée et insertion de ces données là.

Produit : Création & Insertion.

Problèmes : Faire une requête à la base de donnée.

##Jour 3 "Welcome to Hell" (08/12)

Prévu : refaire un nouveau diagramme, nouvelle base de donnée, db visible, reprendre la structure de data.sql.

Produit : correction du diagramme, création et insertion de la db(enfin visible et utilisable), structure    correcte.

Problèmes : tableau vide, diagramme erroné(pk sur tous les id), structure avec la mauvaise syntaxe.

##Jour 4 "Journée mélancolique" (09/12)

Prévu : Afficher les compétences(titre+content), afficher les critères dans les compétences correspondantes à partir de la base de donnée.

Produit : changement du diagramme avec un nouvel id dans la table pivot, affichage des compétences avec leurs critères suivant leurs id, affichage des titres des compétences sur la page d'accueil, création d'une page glossaire technique.

Problèmes : petit soucis au niveau des chemins des id pour chaque compétences.

##Jour 5 (10/12)

Prévu : créer un formulaire, faire des checks-box, insérer un utilisateur, récupérer l'id des critères validés.

Produit : création du formulaire avec les checks-box pour chaque critère, récupération des id critères dans un tableau.

Problèmes : user non visible dans sa table, récupérer les id des checks-box.

##Jour 6 (13/12)

Prévu : insérer dans la table pivot, les critères cochés par l'utilisateur.

Produit : modification de la checkbox pour qu'elle renvoie une clé valeur, puis insertion dans la table pivot des critères cochés par l'user.

Problèmes : echec de la réception dans le cas d'un seul critère coché, paramètres non reçu dans la table pivot.



